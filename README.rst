Python Module Skeleton
======================

A simple Python module skeleton used to help assist in building modules.

Usage
-----

Download this repository and adjust where appropriate

License
-------

Uses the `MIT`_ license.


.. _MIT: http://opensource.org/licenses/MIT
