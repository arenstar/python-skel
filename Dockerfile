FROM python:3-onbuild

RUN apt-get -y update && apt-get -y install gunicorn && apt-get -y autoremove && rm -rf /var/lib/apt/lists/*

EXPOSE 80

CMD gunicorn --bind 0.0.0.0:80  wsgi
