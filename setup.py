from setuptools import setup, find_packages

setup(name='python-skel',
      version='1.0.7',
      description='A python application skeleton',
      long_description='This represents a sample Python module skeleton used to help assist in making Python projects more easily.',
      author='David Arena',
      author_email='arenstar@gmail.com',
      packages=find_packages(exclude=["tests"]),
      test_suite="smashdocs_core.tests",
      zip_safe=True)
